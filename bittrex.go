package main

import (
	"github.com/thebotguys/signalr"
	"encoding/json"
	"os"
	"fmt"
)

const (
	WS_BASE = "socket.bittrex.com"
	WS_HUB = "CoreHub"
)

func main() {
	config := loadConfig()
	client := signalr.NewWebsocketClient()
	arg := os.Args
	needPair := arg[1]
	order := arg[2]
	stopLoss := arg[3]

	var marketSummary MarketSummary
	handlers := map[string]Handler{}

	client.OnClientMethod = func(hub string, method string, messages []json.RawMessage) {
		if hub != "CoreHub" || method != "updateSummaryState" {
			return
		}

		if err := json.Unmarshal(messages[0], &marketSummary); err != nil {
			return
		}
		for _, tick := range marketSummary.Deltas {
			if matchPairs(tick.MarketName, needPair) && matchVolume(tick, config) {
				if (tick.Last > order) {

				}
				fmt.Println(tick)
				addHandler(tick, handlers, config)
			}
		}

	}

	client.Connect("https", WS_BASE, []string{WS_HUB})

	for {
		client.CallHub(WS_HUB, "SubscribeToExchangeDeltas")
	}

}

func loadConfig() (Config) {
	file, _ := os.Open("conf.json")
	decoder := json.NewDecoder(file)
	config := Config{}
	decoder.Decode(&config)
	return config
}


func matchPairs(currentPair string, needPair string) bool {
	if currentPair == needPair {
		return true
	}
	return false
}

func matchVolume(tick Tick, config Config) bool {
	if tick.Volume > config.Volume {
		return true
	}
	return false
}

func addHandler(tick Tick, handlers map[string]Handler, config Config)  {
	if inHandlersMap(tick.MarketName, handlers) {
		handler := handlers[tick.MarketName]
		handler.Insert(tick)
		handlers[tick.MarketName] = handler
		go handler.Action(tick)
	} else {
		handler := Handler{MarketName: tick.MarketName, Config: config}
		handler.Insert(tick)
		handlers[tick.MarketName] = handler
	}
}

func inHandlersMap(name string, handlers map[string]Handler) bool {
	_, ok := handlers[name]
	if ok {
		return true
	}
	return false
}