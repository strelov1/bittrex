package main

import (
	"fmt"
	"github.com/toorop/go-bittrex"
)

const (
	API_KEY    = "d1f04fd4189b4a449ba10da3b5647884"
	API_SECRET = "c93dd51a1dc2455ea1e5b0f69020a500"
)

func main() {
	bittrex := bittrex.New(API_KEY, API_SECRET)

	deposits, err := bittrex.GetDepositHistory("all")
	if err != nil {
		return
	}

	withdrawals, err := bittrex.GetWithdrawalHistory("all")
	if err != nil {
		return
	}

	currenciesDeposit := map[string]float64{}
	for _, deposit := range deposits {
		if isCurrenciesMap(deposit.Currency, currenciesDeposit) {
			amount := currenciesDeposit[deposit.Currency] + deposit.Amount
			currenciesDeposit[deposit.Currency] = amount
		} else {
			currenciesDeposit[deposit.Currency] = deposit.Amount
		}
	}
	//fmt.Println("Deposit", currenciesDeposit)

	currenciesWithdrawals := map[string]float64{}
	for _, withdrawal := range withdrawals {
		if isCurrenciesMap(withdrawal.Currency, currenciesWithdrawals) {
			amount := currenciesWithdrawals[withdrawal.Currency] + withdrawal.Amount
			currenciesWithdrawals[withdrawal.Currency] = amount
		} else {
			currenciesWithdrawals[withdrawal.Currency] = withdrawal.Amount
		}
	}
	//fmt.Println("Withdrawals", currenciesWithdrawals)

	currenciesReal := map[string]float64{}
	for currency, deposit := range currenciesDeposit {
		if isCurrenciesMap(currency, currenciesWithdrawals) {
			amount := deposit - currenciesWithdrawals[currency]
			currenciesReal[currency] = amount
		} else {
			currenciesReal[currency] = deposit
		}
	}

	var allSum float64
	for currency, amount := range currenciesReal {
		if currency != "BTC" {
			pair := "BTC-" + currency
			marketSummary, err := bittrex.GetMarketSummary(pair)
			if err != nil {
				return
			}
			allSum += amount * marketSummary[0].Last
			//fmt.Println(currency, amount, marketSummary[0].Last, amount * marketSummary[0].Last)
		}
	}
	fmt.Println(allSum)

}

func isCurrenciesMap(name string, currencies map[string]float64) bool {
	_, ok := currencies[name]
	if ok {
		return true
	}
	return false
}