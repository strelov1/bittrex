package main

import "log"

type Handler struct {
	MarketName string
	Config Config
	timeSeries []Tick
}

func (self *Handler) Insert(tick Tick) {
	self.timeSeries = append(self.timeSeries, tick)
}

func (self *Handler) Action(newTick Tick) {
	for _, previousTick := range self.timeSeries {
		if newTick.Last > previousTick.Last && self.detectTime(previousTick, newTick) && self.detectPercent(previousTick, newTick) {
			log.Println(previousTick.MarketName, previousTick.Last, newTick.Last)
		}
	}
	self.Clean(newTick)
}

func (self *Handler) Clean(tick Tick)  {
	if self.timeSeries[0].TimeStamp.unix <= tick.TimeStamp.unix - (self.Config.DetectTime + 1) * 60 {
		self.timeSeries = self.timeSeries[1:]
	}
}

func (self *Handler) detectPercent(previousTick Tick, newTick Tick) bool {
	differencePercent := countPercent(previousTick, newTick)
	if differencePercent > 0 && differencePercent > self.Config.DetectPercent {
		return true
	}
	return false
}

func (self *Handler) detectTime(previousTick Tick, newTick Tick) bool {
	if previousTick.TimeStamp.unix <= newTick.TimeStamp.unix - self.Config.DetectTime * 60 {
		return true
	}
	return false
}

func countPercent(previousTick Tick, newTick Tick) float64 {
	return ((newTick.Last / previousTick.Last) - 1) * 100
}
