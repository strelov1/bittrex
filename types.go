package main

import (
	"encoding/json"
	"time"
)

const TIME_FORMAT = "2006-01-02T15:04:05"

type Config struct {
	Volume float64
	DetectTime int64
	DetectPercent float64
}

type MarketSummary struct {
	Deltas []Tick
}

type Tick struct {
	MarketName     string  `json:"MarketName"`
	Volume         float64 `json:"Volume"`
	Last           float64 `json:"Last"`
	TimeStamp      jTime   `json:"TimeStamp"`
}


//type Tick struct {
//	MarketName     string  `json:"MarketName"`
//	High           float64 `json:"High"`
//	Low            float64 `json:"Low"`
//	Ask            float64 `json:"Ask"`
//	Bid            float64 `json:"Bid"`
//	OpenBuyOrders  int     `json:"OpenBuyOrders"`
//	OpenSellOrders int     `json:"OpenSellOrders"`
//	Volume         float64 `json:"Volume"`
//	Last           float64 `json:"Last"`
//	BaseVolume     float64 `json:"BaseVolume"`
//	PrevDay        float64 `json:"PrevDay"`
//	TimeStamp      jTime   `json:"TimeStamp"`
//}

type jTime struct {
	unix int64
}

func (jt *jTime) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return err
	}
	t, err := time.Parse(TIME_FORMAT, s)
	if err != nil {
		return err
	}
	jt.unix = t.Unix()
	return nil
}

